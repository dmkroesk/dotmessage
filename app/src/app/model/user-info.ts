export class UserInfo {
  constructor(
    public username: string,
    public email: string,
    public signature: string ) {
  }
}
