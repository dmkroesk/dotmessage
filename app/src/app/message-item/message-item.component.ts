import {Component, Input, OnInit} from '@angular/core';
import {DotMessage} from '../model/dot-message';

@Component({
  selector: 'app-message-item',
  templateUrl: './message-item.component.html',
  styleUrls: ['./message-item.component.css']
})
export class MessageItemComponent implements OnInit {

  @Input() message: DotMessage;

  constructor() { }

  ngOnInit() {
  }

}
