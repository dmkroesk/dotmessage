import {RouterModule, Routes} from '@angular/router';
import {UserInfoComponent} from './user-info/user-info.component';
import {NgModule} from '@angular/core';
import {UserInfoGuardService} from './user-info-guard.service';
import {MessagesComponent} from './messages/messages.component';

const appRoutes: Routes = [
  { path: '', component: UserInfoComponent },
  { path: 'messages', component: MessagesComponent, canActivate: [UserInfoGuardService]},
  { path: '**', redirectTo: '/' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule {
}
