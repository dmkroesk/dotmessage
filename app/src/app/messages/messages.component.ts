import { Component, OnInit, OnDestroy } from '@angular/core';
import { DotMessage } from '../model/dot-message';
import { DotMessageService } from '../services/dot-message.service';
import {trigger, style, transition, animate, query, state} from '@angular/animations';
import {UserInfo} from '../model/user-info';
import {UserInfoService} from '../services/user-info.service';
import * as socketIo from 'socket.io-client';

import {forEach} from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css'],
  animations: [
    trigger('list', [

      state('in', style({
        opacity: 1,
      })),

      transition('void => *', [
        style({
          opacity: 0,
        }),
        animate('250ms ease-in')
      ]),
    ]),
  ]
})

export class MessagesComponent implements OnInit, OnDestroy {

  messages: DotMessage[] = [];
  user: '';
  fadeIn = 'in';
  socket;
  //
  subject;

  constructor(private dotMessageService: DotMessageService,
              private userInfoService: UserInfoService) {
  }

  //
  //
  //
  ngOnInit() {

    // Get current username
    this.user = this.userInfoService.get().username;

    // Init websockets and update subjects
    this.socket = socketIo('ws://192.168.2.45:8081?signature=' + this.userInfoService.get().signature);
      this.socket.on('DotMessage', (msg) => {
      const fdm = new DotMessage(msg.name, msg.email, msg.message, msg.signature, msg.createdAt);
      this.dotMessageService.dotMessage.next(fdm);
    });

    //
    this.subject = this.dotMessageService.dotMessage.subscribe(
      (msg: DotMessage) => {
        if (this.messages.length >= 3) {
          this.messages.splice(0, 1);
        }
        this.messages.push(msg);
      }
    );

    // Display last 3 chat items
    this.getMostRecentMessages();
  }

  //
  //
  //
  getMostRecentMessages() {
    this.dotMessageService.getMsg()
      .subscribe(
        (msgs: DotMessage[]) => {
          msgs.reverse().map( msg => this.messages.push(msg));
        },
        (error) => {
          console.log(error);
        });
  }

  //
  //
  //
  ngOnDestroy() {

    // Close socket if open
    if (this.socket) {
      this.socket.close();
    }

    // Unsubscribe
    this.subject.unsubscribe();
  }
}
