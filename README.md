
# Dot Message board (Proof of concept, MEAN stack, MQTT, WS, ESP32, HT16K33) #

We all use WhatsApp group messages and that’s fine. But wouldn’t it be great to see your  group messages on a physical message board? And wouldn’t it be handy that selectable (AI) services push information to your groups message board?

The message board platform is built with 2018-IoT-Technology-That-Matters including MQTT, WebSocket’s, Restful, ESP32 and MEAN stack. All kinds of displays can be interfaced.

[![Alt text](https://img.youtube.com/vi/SYSjgnGJuGM/0.jpg)](https://www.youtube.com/watch?v=SYSjgnGJuGM)

and with cheap LED matrix boards (scrolling over multiple modules took some effort ;)

[![Alt text](https://img.youtube.com/vi/6p_Dj9Kuoa4/0.jpg)](https://youtu.be/6p_Dj9Kuoa4)

##The overall picture##

###ESP32###

The ESP32-Pico-Kit V4 (ESP32) drives a display (the HCI part). In this repository implementation code for a retro Hanover flip-dot display and a modern LED matrix display is given. (The LED matrix display is a collection of Adafruit’s HT16K33 led matrix boards). The LED display has scrolling capabilities, the flipdot has more pixels. Bottom line is that the ESP32 can communicate messages to humans 😉
Every ESP32 generates an unique signature that is shown at powerup and during the internet setup procedure. With this unique signature the ESP32 connects to a MQTT broker (can be set during internet setup) and subscribes to a ‘raw’ and a ‘msg’ topic. Internet setup is done with the excellent WiFiManager package.
During boot the ESP32 communicates all settings to the serial console, e.g.:  
setup:mqtt_username: 
setup:mqtt_password: 
setup:mqtt_id: HFD_112x16_01_401DA0D8
setup:mqtt_topic RAW: DotMessage/HFD-401DA0D8/raw
setup:mqtt_topic MSG: DotMessage/HFD-401DA0D8/msg
setup:mqtt_remote_host: test.mosquitto.org
setup:mqtt_remote_port: 1883
mqtt_connect: MQTT connected to test.mosquitto.org:1883
The message topic is for publishing text-messages, the raw topic is for displaying bitmaps (images)
The webapp
A backend serves a web-app using MEAN stack technology. A very basic Bootstrap based UI makes the internals the Angular 5 frontend more understandable.
The web app comes with two components. One to gather name, email and the unique signature, the other to send messages. Navigating to the latter UI is only possible when the user inputs valid information. The components are built with very strict checks on user inputs using Angular 5 reactive forms and input validation using regular expressions. Input data is saved in the Angular localStorage.
When navigation to the Message component connected clients also connects to the backend’s websocket interface. This makes it possible to instantaneous update all DOM’s of all connected clients. This means that send messages show up on all client with the same ESP32 signature (within the same group).
At startup the Message component retrieves the last 3 send messages using a restful api call and render the result in the client’s DOM.
The backend
The Node.js backend connects at boot to a local installed Mongo database and stores all send messages. The web-app sends messages by restful POST api calls. The backend also publishes all posted messages to a connected MQTT broker meaning all subscribed ESP’s can display posted messages.
The node backend also acts as a webserver using Express to serve the Angular 5 webapp


