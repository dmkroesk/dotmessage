#ifndef _FLIPDOT_FONT_H
#define _FLIPDOT_FONT_H

typedef struct font {
  uint8_t height;
  uint8_t width;
  const uint8_t *fontMap;
} FONT_STRUCT;

extern const uint8_t unscii_font[];
extern const FONT_STRUCT unscii;

#endif
